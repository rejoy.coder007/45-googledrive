package com.google.android.gms.drive.sample.driveapimigration.imagevideo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.drive.sample.driveapimigration.HomeActivity;
import com.google.android.gms.drive.sample.driveapimigration.R;
import com.google.android.gms.drive.sample.driveapimigration.imagevideo.lib.LibPicker;
import com.zhihu.matisse.Matisse;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.util.List;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


/**
 * Created by alhazmy13 on 3/13/17.
 */

public class VideoFragment extends Fragment {
    private VideoView videoView;
    private TextView path;

    private static final String TAG = "ImageVideoPicker";
    private List<String> mPath;
    List<Uri> mSelected;
    public LibPicker libPicker;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_layout, container, false);
        init(view);
        libPicker = new LibPicker();

        return view;
    }

    private void init(View view) {
        // Find our View instances
        videoView = view.findViewById(R.id.iv_video);
        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        path = view.findViewById(R.id.tv_path);
        view.findViewById(R.id.bt_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickVideo();
            }
        });
    }


    private void pickVideo() {


        new VideoPicker.Builder(getActivity())
                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                .directory(VideoPicker.Directory.DEFAULT)
                .extension(VideoPicker.Extension.MP4)
                .enableDebuggingMode(true)
                .build();
      //  libPicker.askPermission();
    }

    //
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            mPath = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
            loadVideo();
        }

        if (requestCode == libPicker.PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);
            loadVideo_1();
        }


    }

    private void loadVideo() {
        if (mPath != null && mPath.size() > 0) {

            Uri uri = Uri.parse(mPath.get(0));


            //  imageView.setImageBitmap(BitmapFactory.decodeFile(mPath.get(0)));
            Log.d("UPLOAD", mPath.get(0).toString());
            Log.d("UPLOAD", uri.toString());




            HomeActivity.mDriveServiceHelper_Img.uploadFile(UUID.randomUUID().toString(),uri,getMimeType(getActivity(), uri),getActivity());
            Log.d("UPLOAD", "MIME -- "+getMimeType(getActivity(), uri) );

            path.setText(mPath.get(0));
           // videoView.setVideoURI(Uri.parse(mPath.get(0)));
            videoView.setVideoURI(uri);
            videoView.start();
        }
    }


    private void loadVideo_1() {
        if (mSelected != null && mSelected.size() > 0) {

            Uri uri = Uri.parse(mSelected.get(0).toString());


            //  imageView.setImageBitmap(BitmapFactory.decodeFile(mPath.get(0)));
            Log.d("UPLOAD", mSelected.get(0).toString());
            Log.d("UPLOAD", uri.toString());




           // HomeActivity.mDriveServiceHelper_Img.uploadFile(UUID.randomUUID().toString(),uri,getMimeType(getActivity(), uri),getActivity());
            Log.d("UPLOAD", "MIME -- "+getMimeType(getActivity(), uri) );

            path.setText(mSelected.get(0).toString());
            // videoView.setVideoURI(Uri.parse(mPath.get(0)));
            videoView.setVideoURI(uri);
            videoView.start();
        }
    }
    public String getMimeType(Context context, Uri uri) {
        String mimeType = null;
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }
}
