package com.google.android.gms.drive.sample.driveapimigration.imagevideo;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.drive.sample.driveapimigration.DriveServiceHelper;
import com.google.android.gms.drive.sample.driveapimigration.HomeActivity;

import com.google.android.gms.drive.sample.driveapimigration.R;
import com.google.api.services.drive.model.File;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import java.util.UUID;

/**
 * Created by alhazmy13 on 3/13/17.
 */

public class ImageFragment extends Fragment {
    private ImageView imageView;
    private TextView path;

    private static final String TAG = "ImageVideoPicker";
    private List<String> mPath;

    private DriveServiceHelper mDriveServiceHelper;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_layout, container, false);

        // Find our View instances
        imageView = view.findViewById(R.id.iv_image);
        path = view.findViewById(R.id.tv_path);
        view.findViewById(R.id.bt_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        return view;
    }


    private void pickImage() {
        new ImagePicker.Builder(getActivity())
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .allowMultipleImages(true)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .allowOnlineImages(false)
                .scale(600, 600)
                .allowMultipleImages(true)
                .enableDebuggingMode(true)
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK)
        {
            mPath = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            Log.d(TAG, "onActivityResult: ");
            loadImage();
        }
    }

    private void loadImage() {
        Log.d(TAG, "loadImage: " + mPath.size());
        if (mPath != null && mPath.size() > 0) {
            path.setText(mPath.get(0));

          //  Log.d("UPLOAD", mPath.get(0).toString());

          //  Uri uri = Uri.fromFile(new File(mPath.get(0).toString()));
//Uri uri = Uri.parse("content://media/external/images/media/6562");
            //Uri uri = Uri.parse(new File("content:/"+mPath.get(0)).toString());

           Uri uri = Uri.parse(mPath.get(0));


          //  imageView.setImageBitmap(BitmapFactory.decodeFile(mPath.get(0)));
            Log.d("UPLOAD", mPath.get(0).toString());
            Log.d("UPLOAD", uri.toString());


          //  getMimeType
            imageView.setImageURI(uri);

            HomeActivity.mDriveServiceHelper_Img.uploadFile(UUID.randomUUID().toString(),uri,getMimeType(getActivity(), uri),getActivity());
            Log.d("UPLOAD", "MIME -- "+getMimeType(getActivity(), uri) );
        }
    }

    public String getMimeType(Context context, Uri uri) {
        String mimeType = null;
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public String getMimeType1(String filePath) {
        String type = null;
        String extension = null;
        int i = filePath.lastIndexOf('.');
        if (i > 0)
            extension = filePath.substring(i+1);
        if (extension != null)
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        return type;
    }

}
